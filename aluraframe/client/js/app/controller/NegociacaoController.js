class NegociacaoController {

    constructor(){
        let $ = document.querySelector.bind(document);
        this._inputquantidade = $("#quantidade");
        this._inputvalor = $("#valor");
        this._inputdata = $("#data");
    }
    adiciona(event){

        event.preventDefault();

        console.log(typeof (this._inputdata.value));

        // Formas de Trabalhar com Data;

        //let data = new Date(this._inputdata.value.split('-'));
        //let data = new Date(this._inputdata.value.replace(/-/g, ','));
        //console.log(data);

        let data = new Date(...
            this._inputdata.value
            .split('-')
            .map((item,indice) => item - indice % 2 )
        );

        let negociacao = new Negociacao(data, this._inputquantidade.value,
            this._inputvalor.value);

        console.log(negociacao);

    }
}