class Pessoa{

    constructor(nome, sobrenome, idade, cpf){
        this._nome = nome;
        this._sobrenome = sobrenome;
        this._idade = idade;
        this._cpf = cpf;
        Object.freeze(this);
    }

    get nomeCompleto(){
        return this._nome + " " + this._sobrenome;
    }

    get nome() {
        return this._nome;
    }

    get sobrenome() {
        return this._sobrenome;
    }

    get idade() {
        return this._idade;
    }

    get cpf() {
        return this._cpf;
    }
}