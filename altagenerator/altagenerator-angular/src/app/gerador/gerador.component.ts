import { Component, OnInit } from '@angular/core';
import {Gerador} from "./gerador";

@Component({
  selector: 'app-gerador',
  templateUrl: './gerador.component.html',
  styleUrls: ['./gerador.component.css']
})
export class GeradorComponent implements OnInit {

  gerados = [];

  gerador: Gerador = {
    nome: ''
  };

  addWord() {
    return this.gerados.push(this.gerador.nome.concat('miro'));
  }

  constructor() { }

  ngOnInit() {
  }

}
